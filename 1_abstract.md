# immobot.io

__immobot.io__ est le courtier immobilier digital dédié à l’investissement locatif. Grâce à lui trouver les meilleurs investissements n’a jamais été aussi simple et rapide. Remplissez votre profil en quelques clics et recevez immédiatement les meilleures opportunités d’investissements ciblées. Que ce soit pour préparer sa retraite, optimiser sa fiscalité ou construire un patrimoine, __immobot.io__ s’adapte à toutes les stratégies. 
